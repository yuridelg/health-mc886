# Trabalho 3

## Getting Started

O conteúdo do arquivo ZIP é dado por um arquivo Main.ipynb no formato Jupyter Notebook, um diretório output contendo os resultados dos experimentos realizados, um diretório files com os arquivos de entrada utilizados, um relatório do trabalho, e este arquivo de instruções README.

### Prerequisites

É necessário possuir o sistema Jupyter Notebook configurado para a execução do arquivo Main.ipynb.

## Running the tests

A execução dos experimentos realizados está explicitada em células do arquivo Main.ipynb, bem como descrita no relatório em anexo. Basta-se executar o arquivo Main.ipynb de forma sequencial em um sistema Jupyter. Os resultados dos experimentos realizados e analisados no relatório estão em anexo no diretório output. Note: a execução do programa Main.ipynb pode sobreescrever os arquivos já existentes sob o diretório output.
